extends Area2D


signal asteroidHit


export var speed : float = 200

var velocity := Vector2(1, 0)

# TODO: ensure velocity is normalized? (Sanity check)


func _process(delta: float) -> void:
	position += velocity * speed * delta


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


func _on_Bullet_area_entered(other: Area2D) -> void:
	if other.is_in_group("Asteroids"):
		emit_signal("asteroidHit", self, other)
