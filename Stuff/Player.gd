extends Area2D


# TODO: move to utils lib:
func dir_from_pressed(positive_action : String, negative_action : String) -> int:
	return int(Input.is_action_pressed(positive_action)) - int(Input.is_action_pressed(negative_action))


signal crashed
signal shoot

export var acc : float = 250.0			# px/s/s
export var max_speed : float = 500.0	# px/s

var velocity := Vector2(0, 0)			# (px/s, px/s)


export var rotation_speed : float = 2.0	# r/s


func _ready():
	pass

func _process(delta: float) -> void:
	# Rotation 				####
	rotation += dir_from_pressed("ui_right", "ui_left") * rotation_speed * delta
	
	# Acceleration 			####
	var dir := Vector2(1, 0).rotated(rotation)
	velocity += acc * dir * int(Input.is_action_pressed("ui_up")) * delta
	
	
	position += velocity * delta
	
	# Wrap if out of bounds
	var vp_rect : Vector2 = get_viewport_rect().size
	var margin : float = 32 # TODO: this could probably be moved to a better place (Duplicate in all the places...)
	
	position.x = wrapf(position.x, -margin, vp_rect.x + margin)
	position.y = wrapf(position.y, -margin, vp_rect.y + margin)
	
	
	# Shooting ##########
	if Input.is_action_just_pressed("player_shoot"):
		$ShootSFX.pitch_scale = 1 + rand_range(-0.25, 0.25)
		$ShootSFX.play()
		emit_signal("shoot", position + $BulletSpawnOffset.position.rotated(rotation))



func _on_Player_area_entered(area: Area2D) -> void:
	if area.is_in_group("Asteroids"):
		emit_signal("crashed")
