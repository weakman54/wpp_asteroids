extends Area2D


# TODO: move into util lib (and make variations etc)
# Returns a vec2 of random length (within given parameters) and random rotation (0, TAU)
func rand_vec2(minval : float, maxval : float) -> Vector2:
	return Vector2(rand_range(minval, maxval), 0).rotated(rand_range(0, TAU))


const Min_Speed : float = 50.0
const Max_Speed : float = 300.0				# px/s

const Max_Rotation_Speed : float = 3.0		# r/s

var velocity := Vector2()					# (px/s, px/s)
var rotation_speed : float = 2.0			# r/s

var size : int = 0

func _init() -> void:
	velocity = rand_vec2(Min_Speed, Max_Speed)
	rotation_speed = rand_range(-Max_Rotation_Speed, Max_Rotation_Speed)

#func _ready():
	
#	printt("asteroid ready:", velocity, rotation_speed)

func _process(delta: float) -> void:
	# Set position (Wrap if out of bounds)
	var vp_rect : Vector2 = get_viewport_rect().size
	var margin : float = 32 # TODO: this could probably be moved to a better place
	
	position += velocity * delta
	
	position.x = wrapf(position.x, -margin, vp_rect.x + margin)
	position.y = wrapf(position.y, -margin, vp_rect.y + margin)
	
	rotation += rotation_speed * delta
	
	
