extends Control

signal play_pressed
signal exit_pressed





func _on_Btn_Play_pressed() -> void:
	emit_signal("play_pressed")


func _on_Btn_Exit_pressed() -> void:
	emit_signal("exit_pressed")


func _on_GameOver_visibility_changed() -> void:
	if visible:
		$VBoxContainer/Btn_Play.grab_focus()
