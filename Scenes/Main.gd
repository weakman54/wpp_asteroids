extends Node

var gameOver : bool = true


func resetGame() -> void:
	gameOver = true

func runGame() -> void:
	gameOver = false


func _ready() -> void:
	randomize()
	
	$Game.reset()
	get_tree().paused = true


func _process(_delta : float) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		runGame()


func _on_GameOver_exit_pressed() -> void:
	get_tree().quit()


func _on_GameOver_play_pressed() -> void:
	$Game.reset()
	$GameOver.hide()
	get_tree().paused = false


func _on_Game_gameOver() -> void:
	$GameOver.show()
	get_tree().paused = true
