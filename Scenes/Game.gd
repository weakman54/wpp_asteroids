extends Node2D

# TODO: move into util lib (and make variations etc)
# Returns a vec2 of random length (within given parameters) and random rotation (0, TAU)
func rand_vec2(minval : float, maxval : float) -> Vector2:
	return Vector2(rand_range(minval, maxval), 0).rotated(rand_range(0, TAU))


signal gameOver

export var Bullet : PackedScene


export var asteroid_arrs : Array


func reset() -> void:
	# Reset Player properties ########
	# TODO: HARDCODED Position, should be set to screen size
	$Player.position.x = 1024/2
	$Player.position.y = 600/2 
	
	$Player.rotation = 0
	
	$Player.velocity.x = 0
	$Player.velocity.y = 0
	
	# Reset Asteroid timer ######
	$Timer_SpawnAsteroid.start($Timer_SpawnAsteroid.wait_time)
	
	# Remove Asteroids ########
	for instAsteroid in $Asteroids.get_children():
		instAsteroid.queue_free()
	
	
	# Remove Bullets ########
	for instBullet in $Bullets.get_children():
		instBullet.queue_free()
	
	

func _ready():
	pass


func _on_Player_crashed() -> void:
	emit_signal("gameOver")


func _on_Timer_SpawnAsteroid_timeout() -> void:
	# Asteroid size ######
	# TODO: figure out better way to select asteroid size (keeping in mind weighting and so on)
	var asteroid_size : int = randi() % asteroid_arrs.size()
	var asteroid_arr : Array = asteroid_arrs[asteroid_size]
		
	# Asteroid spawn position ####	
#		var margin : float = 32 # TODO: duplicated in asteroid code and could be in better place probably
	var screen_size : Vector2 = get_viewport_rect().size
	
	var spawn_octant : Vector2 = rand_vec2(0, 1).round()
	
	while spawn_octant.length() == 0: 	# NOTE: Potential (but unlikely) infinite loop. TODO: add some number of tries before default is chosen
										# Testing puts this at 20 tries being a good point, It went through thousands of tries and never got above 15 tries
		spawn_octant = rand_vec2(0, 1).round()
	
	var spawn_position : Vector2 = screen_size/2 + (screen_size/2 + Vector2(100, 100)) * spawn_octant
	
	var inst : Area2D = asteroid_arr[randi() % asteroid_arr.size()].instance()
	
	inst.position = spawn_position
	inst.size = asteroid_size
	inst.add_to_group("Asteroids")
	
	$Asteroids.add_child(inst)


func _on_Bullet_asteroidHit(bullet : Area2D, asteroid : Area2D) -> void:	
	bullet.queue_free()
	asteroid.queue_free()
	
	$SFXAsteroidExplode.play()
	
	if asteroid.size != 0: # FIXME: kindof hardcoded and brittle, but not sure how to fix...
		var new_size : int = asteroid.size - 1
		var asteroid_arr : Array = asteroid_arrs[new_size]
		
		for _i in range(1 + randi()%3):
			var inst : Area2D = asteroid_arr[randi() % asteroid_arr.size()].instance()
			
			inst.velocity = asteroid.velocity
			inst.velocity = inst.velocity.rotated(rand_range(-PI/4, PI/4))
			inst.velocity *= rand_range(0.75, 0.9)
			
			inst.position = asteroid.position
			inst.size = new_size
			inst.add_to_group("Asteroids")
			
			$Asteroids.add_child(inst)



func _on_Player_shoot(bulletSpawnPos : Vector2) -> void:
#	prints("Shoot", bulletSpawnPos)
	var instBullet = Bullet.instance()
	
	instBullet.position = bulletSpawnPos
	instBullet.rotation = $Player.rotation
	
	instBullet.velocity = instBullet.velocity.rotated(instBullet.rotation)
	
	instBullet.connect("asteroidHit", self, "_on_Bullet_asteroidHit")
	
	$Bullets.add_child(instBullet)
	# Instantiate bullet
	# Calculate trajectory and such
	# Add to scene
	pass # Replace with function body.
